import com.notnoop.apns.APNS;


public class TestPlayLoader {
	public static void main(String[] args) {
		String payload = APNS.newPayload()
				.customField("msg", "123")
				.instantDeliveryOrSilentNotification()
				.alertBody(null)
				.alertTitle(null)
				.build();
		
		System.out.println(payload);
	}
}
