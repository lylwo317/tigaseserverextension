package cn.rspread.xmppserver.push;

import java.util.Map;

import tigase.conf.ConfigurationException;
import tigase.server.Message;
import tigase.xmpp.BareJID;

public interface PushProvider {

	public void init(Map<String, Object> properties)
			throws ConfigurationException;

	public String getName();

	// for service discovery
	public String getNode();

	public String getJidPrefix();

	public String getDescription();

	public void sendPushNotification(String pushName, String message,String msgBody,
			PushRegistrationInfo info);
}
