package cn.rspread.xmppserver.push;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import cn.rspread.xmppserver.TalkKingSetPushName;
import tigase.conf.ConfigurationException;
import tigase.db.DBInitException;
import tigase.db.RepositoryFactory;
import tigase.db.TigaseDBException;
import tigase.db.UserRepository;
import tigase.server.AbstractMessageReceiver;
import tigase.server.Iq;
import tigase.server.Message;
import tigase.server.Packet;
import tigase.util.TigaseStringprepException;
import tigase.xml.Element;
import tigase.xmpp.BareJID;
import tigase.xmpp.JID;
import tigase.xmpp.StanzaType;

public class TalkKingPushComponent extends AbstractMessageReceiver {
	private static Logger log = Logger.getLogger(TalkKingPushComponent.class
			.getName());

	private static final String DISCO_DESCRIPTION = "TalkKing push notifications.";
	private static final String NODE = "http://talk-king.net/extensions/push";
	private static final String XMLNS = NODE;
	private static final Element top_feature = new Element("feature",
			new String[] { "var" }, new String[] { NODE });
	private static final List<Element> DISCO_FEATURES = Arrays
			.asList(top_feature);
	private static final int NUM_THREADS = 20;

	private PushProvider pushProvider = new ApnsProvider();
	private PushRepository pushRepository = new DataPushRepository();

	@Override
	public void processPacket(Packet packet) {
		// TODO Auto-generated method stub
		// registration
		if (packet.getElemName().equals(Iq.ELEM_NAME)
				&& packet.getType() == StanzaType.set) {
			Element register = packet.getElement().getChild("register", XMLNS);
			if (register != null
					&& pushProvider.getName().equals(
							register.getAttributeStaticStr("provider"))) {
				String regId = register.getCData();
				if (regId != null && regId.length() > 0) {
					BareJID user = packet.getStanzaFrom().getBareJID();
					log.log(Level.FINE,
							"Registering user {0} to push notifications", user);
					try {
						pushRepository.register(user, pushProvider.getName(),
								regId);
					} catch (TigaseDBException e) {
						log.log(Level.INFO, "Database error", e);
					}

					packet.processedBy(getComponentInfo().getName());
					addOutPacket(packet.okResult((String) null, 0));
				}
			}
		}
		// push notification
		else if (packet.getElemName().equals("pushmessage")) {
			if (packet.getStanzaFrom().toString()
					.equals(getDefVHostItem().getDomain())) {

				Element push = packet.getElement().getChild("message",
						"jabber:client");
				String body = push.getChild("body").getCData();
				if (push != null) {
					String message = push.toString();
					String toJid = push.getAttributeStaticStr(Message.TO_ATT);
					String fromJid = push
							.getAttributeStaticStr(Message.FROM_ATT);
					if (toJid != null && toJid.length() > 0) {
						BareJID user = BareJID.bareJIDInstanceNS(toJid);
						BareJID fromUser = null;
						String uri = System.getProperty("user-db-uri");

						String roomName = packet
								.getAttributeStaticStr("roomName");
						if (roomName != null) {
							JID senderJid;
							try {
								senderJid = JID.jidInstance(fromJid);
								fromUser = BareJID.bareJIDInstance(senderJid.getResource(),user.getDomain().toString());
							} catch (TigaseStringprepException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
						}else {
							fromUser = BareJID.bareJIDInstanceNS(fromJid);
						}
						

						try {
							
							
							String pushName = null;
							UserRepository userRepository = RepositoryFactory
									.getUserRepository(null, uri, null);
							pushName = userRepository.getData(fromUser,
									TalkKingSetPushName.pushNameKey);

							if (pushName == null || ("").equals(pushName)) {
								pushName = fromUser.getLocalpart();
							}
							
							if (roomName!=null) {
								pushName = pushName + " @ \"" + roomName +"\""; 
							}

							log.log(Level.FINE,
									"Sending push notification to {0}", toJid);
							// send push notification

							List<PushRegistrationInfo> infoList = pushRepository
									.getRegistrationInfo(user);
							for (PushRegistrationInfo info : infoList) {
								if (info.getProvider().equals(
										pushProvider.getName())) {
									pushProvider.sendPushNotification(pushName,
											message, body, info);
								}
							}
						} catch (ClassNotFoundException
								| InstantiationException
								| IllegalAccessException | TigaseDBException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}

			}
		}
	}

	@Override
	public int processingInThreads() {
		// TODO Auto-generated method stub
		return NUM_THREADS;
	}

	@Override
	public void setProperties(Map<String, Object> props)
			throws ConfigurationException {
		super.setProperties(props);
		pushProvider.init(props);

		try {
			pushRepository.init(props);
		} catch (DBInitException e) {
			throw new ConfigurationException(
					"unable to initialize push data repository", e);
		}
	}

	@Override
	public String getDiscoDescription() {
		return DISCO_DESCRIPTION;
	}

	@Override
	public String getDiscoCategory() {
		return super.getDiscoCategory();
	}

	@Override
	public List<Element> getDiscoItems(String node, JID jid, JID from) {
		if (NODE.equals(node)) {
			List<Element> list = new ArrayList<Element>(1);
			list.add(new Element("item",
					new String[] { "node", "jid", "name" }, new String[] {
							pushProvider.getNode(),
							pushProvider.getJidPrefix()
									+ getDefVHostItem().getDomain(),
							pushProvider.getDescription() }));
			return list;
		}

		return null;
	}

	@Override
	public List<Element> getDiscoFeatures(JID from) {
		return DISCO_FEATURES;
	}
}
