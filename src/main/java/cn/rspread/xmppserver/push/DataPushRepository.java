package cn.rspread.xmppserver.push;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import tigase.db.DBInitException;
import tigase.db.DataRepository;
import tigase.db.RepositoryFactory;
import tigase.db.TigaseDBException;
import tigase.xmpp.BareJID;

public class DataPushRepository implements PushRepository {

	private static final String CREATE_QUERY_ID = "push-create-query";
	private static final String CREATE_QUERY_SQL_SQLSERVER = "{ call TigUpdatePush(?, ?, ?) }";
	private static final String CREATE_QUERY_SQL_MYSQl = "REPLACE INTO push VALUES (?, ?, ?)";
	private static final String SELECT_QUERY_ID = "push-select-query";
	private static final String SELECT_QUERY_SQL = "SELECT provider,reg_id FROM push WHERE user_id=?";
	private static final String DELECT_QUERY_ID = "push-delect-query";
	private static final String DELECT_QUERY_SQL = "DELETE FROM push WHERE user_id = ? AND provider = ?";

	private DataRepository repo;

	@Override
	public void init(Map<String, Object> properties) throws DBInitException {
		// TODO Auto-generated method stub
		try {
			String dbUri = (String) properties.get("db-uri");
			
			if (dbUri != null) {
				repo = RepositoryFactory.getDataRepository(null, dbUri, null);
				if (dbUri.startsWith("jdbc:mysql")){
					repo.initPreparedStatement(CREATE_QUERY_ID, CREATE_QUERY_SQL_MYSQl);
				}else {
					repo.initPreparedStatement(CREATE_QUERY_ID, CREATE_QUERY_SQL_SQLSERVER);
				}
				repo.initPreparedStatement(SELECT_QUERY_ID, SELECT_QUERY_SQL);
				repo.initPreparedStatement(DELECT_QUERY_ID, DELECT_QUERY_SQL);
			}
		} catch (Exception exception) {
			throw new DBInitException("error initializing push database");
		}
	}

	@Override
	public void unregister(BareJID jid, String provider)
			throws TigaseDBException {
		PreparedStatement pStatement = null;
		try {
			pStatement = repo.getPreparedStatement(jid, DELECT_QUERY_ID);
			pStatement.setString(1, jid.toString());
			pStatement.setString(2, provider);
			pStatement.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void register(BareJID jid, String provider, String registrationId)
			throws TigaseDBException {
		// TODO Auto-generated method stub
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = repo.getPreparedStatement(jid, CREATE_QUERY_ID);
			preparedStatement.setString(1, jid.toString());
			preparedStatement.setString(2, provider);
			preparedStatement.setString(3, registrationId);
			preparedStatement.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public List<PushRegistrationInfo> getRegistrationInfo(BareJID jid)
			throws TigaseDBException {
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			preparedStatement = repo.getPreparedStatement(jid, SELECT_QUERY_ID);
			preparedStatement.setString(1, jid.toString());
			resultSet = preparedStatement.executeQuery();

			List<PushRegistrationInfo> list = new LinkedList<>();
			while (resultSet.next()) {
				String provider = resultSet.getString(1);
				String regId = resultSet.getString(2);
				list.add(new PushRegistrationInfo(provider, regId));
			}
			return list;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			throw new TigaseDBException(e.getMessage(), e);
		} finally {
			repo.release(null, resultSet);
		}
	}

}
