package cn.rspread.xmppserver.push;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import com.notnoop.apns.internal.ApnsFeedbackConnection;

import tigase.conf.ConfigurationException;
import tigase.server.Message;
import tigase.xmpp.BareJID;
import tigase.xmpp.impl.roster.RosterAbstract;

public class ApnsProvider implements PushProvider {

	private static Logger log = Logger.getLogger(ApnsProvider.class.getName());

	public static final String PROVIDER_NAME = "apns";

	private static final String APNS_JID_PREFIX = "apns.push.";
	private static final String APNS_DESCRIPTION = "IOS Messaging push notifications";
	private static final String APNS_DATA_ACTION = "net.talkking.CHECK_MESSAGES";

	private String certPath;
	private String certPwd;
	private ApnsService service;

	/**
	 * 读取配置文件中的
	 */
	@Override
	public void init(Map<String, Object> properties)
			throws ConfigurationException {
		// TODO Auto-generated method stub
		certPath = (String) properties.get("apns-cert-path");
		certPwd = (String) properties.get("apns-pwd");

		if (certPath != null && certPwd != null) {
			service = APNS.newService().withCert(certPath, certPwd)
					.withSandboxDestination().build();
		}
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return PROVIDER_NAME;
	}

	/*@Override
	public void sendPushNotification(String tojid, String message,
			PushRegistrationInfo info) {
		// TODO Auto-generated method stub
		
	}
*/
	@Override
	public String getNode() {
		// TODO Auto-generated method stub
		return certPath != null ? certPath : "unconfigured";
	}

	@Override
	public String getJidPrefix() {
		// TODO Auto-generated method stub
		return APNS_JID_PREFIX;
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return APNS_DESCRIPTION;
	}

	@Override
	public void sendPushNotification(String pushName, String message,
			String msgBody, PushRegistrationInfo info) {
		// TODO Auto-generated method stub
		if (service == null) {
			log.log(Level.WARNING, "Apns provider not configured correctly.");
			return;
		}

		String regId = info.getRegistrationId();
		if (regId != null) {
			/*String payload = APNS.newPayload().alertBody("Somebody send a message to you.")
					.alertTitle("New message").build();*/
			String payload = APNS.newPayload()
					.customField("msg", message)
					.alertBody(pushName + ":\n"+msgBody)
					.alertTitle("New message")
					.build();
			service.push(regId, payload);
		}
	}

}
