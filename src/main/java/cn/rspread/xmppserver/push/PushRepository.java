package cn.rspread.xmppserver.push;

import java.util.List;
import java.util.Map;

import tigase.db.DBInitException;
import tigase.db.TigaseDBException;
import tigase.xmpp.BareJID;

public interface PushRepository {

	public void init(Map<String, Object> properties) throws DBInitException;

	public void unregister(BareJID jid, String provider)
			throws TigaseDBException;

	public void register(BareJID jid, String provider, String registrationId)
			throws TigaseDBException;

	public List<PushRegistrationInfo> getRegistrationInfo(BareJID jid)
			throws TigaseDBException;

}
