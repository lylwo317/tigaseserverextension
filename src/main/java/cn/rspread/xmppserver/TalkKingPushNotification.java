package cn.rspread.xmppserver;

import java.util.Map;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

import tigase.db.NonAuthUserRepository;
import tigase.db.TigaseDBException;
import tigase.server.Message;
import tigase.server.Packet;
import tigase.xml.Element;
import tigase.xmpp.JID;
import tigase.xmpp.StanzaType;
import tigase.xmpp.XMPPPostprocessorIfc;
import tigase.xmpp.XMPPProcessor;
import tigase.xmpp.XMPPResourceConnection;

public class TalkKingPushNotification extends XMPPProcessor implements
		XMPPPostprocessorIfc {
	private static Logger log = Logger.getLogger(TalkKingPushNotification.class
			.getName());
	public static final String XMLNS = "http://talk-king.net/extensions/push";
	public static final String ID = "talkKing:message:push";

	private static final String[][] ELEMENTS = { { Message.ELEM_NAME } };
	private static final String[] XMLNSS = { XMLNS, Message.CLIENT_XMLNS };

	private String componentName;
	private static final Element[] FEATURES = { new Element("push", new String[] { "xmlns" }, new String[] { XMLNS }) };

	@Override
	public void init(Map<String, Object> settings) throws TigaseDBException {
		componentName = (String) settings.get("component");
		if (componentName == null) {
			componentName = "push";
		}
	}

	@Override
	public String id() {
		// TODO Auto-generated method stub
		return ID;
	}

    @Override
    public String[][] supElementNamePaths() {
        return ELEMENTS;
    }

    @Override
    public String[] supNamespaces() {
        return XMLNSS;
    }


	@Override
	public void postProcess(Packet packet, XMPPResourceConnection session,
			NonAuthUserRepository repo, Queue<Packet> results,
			Map<String, Object> settings) {
		if (log.isLoggable(Level.FINEST)) {
            log.finest("Processing packet: " + packet.toString());
        }
		
        if (session == null && packet.getElemName().equals(Message.ELEM_NAME) &&
            packet.getType() == StanzaType.chat && (packet.getElement().getChild("body") != null ||
                packet.getElement().getChild("request", "urn:xmpp:receipts") != null)) {

        	Element request = new Element("pushmessage");
        	request.addChild(packet.getElement());

            // send request to push component
            JID compJid = JID.jidInstanceNS(componentName, packet.getStanzaTo().getDomain(), null);
            JID fromJid = JID.jidInstanceNS(packet.getStanzaTo().getDomain());
            Packet p = Packet.packetInstance(request, fromJid, compJid);
            results.offer(p);
        }
	}
	
	@Override
	public Element[] supStreamFeatures(XMPPResourceConnection session) {
		if (log.isLoggable(Level.FINEST) && (session != null)) {
            log.finest("VHostItem: " + session.getDomain());
        }
        if (session != null) {
            return FEATURES;
        }
        else {
            return null;
        }
	}

}
