package cn.rspread.xmppserver;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import tigase.db.DBInitException;
import tigase.db.NonAuthUserRepository;
import tigase.db.RepositoryFactory;
import tigase.db.TigaseDBException;
import tigase.db.UserNotFoundException;
import tigase.db.UserRepository;
import tigase.server.Iq;
import tigase.server.Packet;
import tigase.util.TigaseStringprepException;
import tigase.xml.Element;
import tigase.xmpp.Authorization;
import tigase.xmpp.BareJID;
import tigase.xmpp.NotAuthorizedException;
import tigase.xmpp.PacketErrorTypeException;
import tigase.xmpp.StanzaType;
import tigase.xmpp.XMPPException;
import tigase.xmpp.XMPPPostprocessorIfc;
import tigase.xmpp.XMPPPreprocessorIfc;
import tigase.xmpp.XMPPProcessor;
import tigase.xmpp.XMPPProcessorIfc;
import tigase.xmpp.XMPPResourceConnection;

public class TalkKingSetPushName extends XMPPProcessor implements
		XMPPProcessorIfc {
	private static Logger log = Logger.getLogger(TalkKingSetPushName.class
			.getName());
	public static final String ID = "talkKing:pushName";
	public static final String pushNameKey = "pushName";
	public static final String XMLNS = ID;
	public static final String[] PUSH_NAME_PATH = new String[] { "iq",
			"pushName" };
	private static final String[][] ELEMENTS = { PUSH_NAME_PATH };
	private static final String[] XMLNSS = { XMLNS };
	private static final Element[] FEATURES = { new Element("user",
			new String[] { "xmlns" }, new String[] { XMLNS }) };

	@Override
	public String id() {
		// TODO Auto-generated method stub
		return ID;
	}

	@Override
	public Set<StanzaType> supTypes() {
		// TODO Auto-generated method stub
		return new HashSet<StanzaType>(Arrays.asList(StanzaType.set));
	}

	@Override
	public String[][] supElementNamePaths() {
		// TODO Auto-generated method stub
		return ELEMENTS;
	}

	@Override
	public String[] supNamespaces() {
		// TODO Auto-generated method stub
		return XMLNSS;
	}

	@Override
	public Element[] supStreamFeatures(XMPPResourceConnection session) {
		// TODO Auto-generated method stub
		if (log.isLoggable(Level.FINEST) && (session != null)) {
			log.finest("VHostItem: " + session.getDomain());
		}
		if (session != null) {
			return FEATURES;
		} else {
			return null;
		}
	}

	@Override
	public void process(Packet packet, XMPPResourceConnection session,
			NonAuthUserRepository repo, Queue<Packet> results,
			Map<String, Object> settings) throws XMPPException {
		// TODO Auto-generated method stub
		if (log.isLoggable(Level.FINEST)) {
			log.finest("Processing packet: " + packet.toString());
		}
		if (session == null) {
			if (log.isLoggable(Level.FINE)) {
				log.log(Level.FINE, "Session is null, ignoring packet: {0}",
						packet);
				return;
			}
			return;
		}

		if (packet.getStanzaFrom() != null
				&& session.isUserId(packet.getStanzaFrom().getBareJID())
				&& !session.isAuthorized()) {
			if (log.isLoggable(Level.FINE)) {
				log.log(Level.FINE,
						"Session is not authorized, ignoring packet: {0}",
						packet);
			}
			return;
		}

		try {
			if (!session.isServerSession() && (packet.getStanzaFrom() != null)
					&& !session.isUserId(packet.getStanzaFrom().getBareJID())) {
				// RFC says: ignore such request
				log.log(Level.WARNING,
						"Roster request ''from'' attribute doesn't match "
								+ "session: {0}, request: {1}", new Object[] {
								session, packet });
				return;
			}

			// <iq from='8618603026485@reason-sz.no-ip.org' id='uwrO8-11'
			// type='set'><pushName
			// xmlns='talkKing:pushName'>谢佳桦Kevin</pushName></iq>
			String xmlns = packet.getElement()
					.getXMLNSStaticStr(PUSH_NAME_PATH);
			if (XMLNS == xmlns && packet.getType() == StanzaType.set) {
				Element pushName = packet.getElement().getChild("pushName",
						XMLNS);
				if (pushName != null) {
					String name = pushName.getCData();
					if (name != null && name.length() > 0) {
						String uri = System.getProperty("user-db-uri");
						UserRepository userRepository = RepositoryFactory
								.getUserRepository(null, uri, null);
						BareJID user = packet.getStanzaFrom().getBareJID();
						log.log(Level.FINE, "Setting {0} push name.", user);
						userRepository.setData(packet.getStanzaFrom()
								.getBareJID(), pushNameKey, name);

						//packet.processedBy(getComponentInfo().getName());
						results.offer(packet.okResult((String) null, 0));
						packet.processedBy(ID);
					}
				}
				//results.offer(packet.errorResult((String) null, (String) null, null, "", includeOriginalXML))
			}

		} catch (NotAuthorizedException e) {
			log.log(Level.WARNING,
					"Received roster request but user session is not authorized yet: {0}",
					packet);
			try {
				results.offer(Authorization.NOT_AUTHORIZED.getResponseMessage(
						packet, "You must authorize session first.", true));
			} catch (PacketErrorTypeException pe) {
				// ignored
			}

		} catch (DBInitException e) {
			// TODO Auto-generated catch block
			log.log(Level.WARNING, "Database problem, please contact admin:", e);
			try {
				results.offer(Authorization.INTERNAL_SERVER_ERROR
						.getResponseMessage(
								packet,
								"Database access problem, please contact administrator.",
								true));
			} catch (PacketErrorTypeException pe) {
				// ignored
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UserNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TigaseDBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
